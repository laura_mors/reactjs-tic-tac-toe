import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

function Square(props) {
  // By calling this.setState in the render method,
  // we tell React to re-render that square whenever the button is clicked
  return (
    <button
      className="square"
      /**
       * When a button is clicked, React calls the onClick event handler
       * that is defined in this render method.
       * The event handler then calls the Square's onClick prop, which is specified by Board
       * The Board passed handleClick(i), which is what Square calls a button is clicked
       */
      onClick={props.onClick}
      style={{backgroundColor: props.bgColor}}
      >
      {props.value}
    </button>
  );
}
  
  class Board extends React.Component {
    renderSquare(i, isWinner) {
      const bgColor = isWinner ? 'yellow' : 'white';
      return (
        <Square 
          key={i.toString()}
          value={this.props.squares[i]}
          bgColor={bgColor}
          onClick={() => this.props.onClick(i)}
          />
      );
    }

    render() {
      const totalRows = 3;
      const totalColumns = 3;
      const boardRows = [];
      
      for (let rowIndex = 0; rowIndex < totalRows; rowIndex++) {
        const row = [];
        for (let squareIndex = rowIndex; squareIndex < rowIndex + totalColumns; squareIndex++) {
          const i = (rowIndex*2)+squareIndex;
          let isWinner = false;
          if (this.props.winningSquares) {
            isWinner = this.props.winningSquares.includes(i);
          }
          row.push(this.renderSquare(i, isWinner));
        }
        boardRows.push(<div className="board-row">{row}</div>);
      }

      return (
        <div>{boardRows}</div>
      );
    }
  }
  
  class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        history: [{
          squares: Array(9).fill(null),
          lastSquareClicked: -1,
        }],
        stepNumber: 0,
        xIsNext: true,
        historyAscOrder: true,
      };
    }

    grid = [
      '(1, 1)', '(1, 2)', '(1, 3)',
      '(2, 1)', '(2, 2)', '(2, 3)',
      '(3, 1)', '(3, 2)', '(3, 3)'
    ];

    handleClick(i) {
      // This ensures that, when jumping to a past move, we throw away all history that happened after it
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      // This makes a copy of the array
      const squares = current.squares.slice();
      if (calculateWinner(squares) || squares[i]) {
        return;
      }
      squares[i] = this.state.xIsNext ? 'X' : 'O';
      this.setState({
        history: history.concat([{
          squares: squares,
          lastSquareClicked: i,
        }]),
        stepNumber: history.length,
        xIsNext: !this.state.xIsNext,
      });
    }

    jumpTo(step) {
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
      });
    }

    toggleMoveListOrder() {
      this.setState({
        historyAscOrder: !this.state.historyAscOrder,
      });
    }

    render() {
      const history = this.state.history;
      const current = history[this.state.stepNumber];
      const winningSquares = calculateWinner(current.squares);

      const sortButtonDesc = this.state.historyAscOrder ? 
                              'Sort moves in descending order' :
                              'Sort moves in ascending order';
      const sortMoves = <button onClick={() => this.toggleMoveListOrder()}>{sortButtonDesc}</button>;

      /**
       * 'step' refers to the current 'history' element value
       * 'move' refers to the current 'history' element index
       */
      const moves = history.map((step, move) => {
        const desc = move ? 
          'Go to move # ' + move + ', position ' + this.grid[history[move].lastSquareClicked] :
          'Go to game start';
        const descElem = (this.state.stepNumber === move) ? <b>{desc}</b> : desc;
        return (
          <li key={move}>
            <button 
              onClick={() => this.jumpTo(move)}>{descElem}</button>
          </li>
        );
      });

      if (!this.state.historyAscOrder) { moves.reverse()}

      let status;
      if (current.squares.includes(null)) {
        if (winningSquares) {
          status = 'Winner: ' + current.squares[winningSquares[0]];
        }
        else {
          status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
      } else {
        status = "It's a draw!";
      }

      return (
        <div className="game">
          <div className="game-board">
            <Board
              squares={current.squares}
              winningSquares={winningSquares}
              onClick={(i) => this.handleClick(i)}/>
          </div>
          <div className="game-info">
            <div>{status}</div>
            <div>{sortMoves}</div>
            <ol>{moves}</ol>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  const root = ReactDOM.createRoot(document.getElementById("root"));
  root.render(<Game />);

  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
  
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return [a, b, c];
      }
    }
  
    return null;
  }
  