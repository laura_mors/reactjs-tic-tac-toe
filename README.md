# ReactJS Tic-Tac-Toe

An interactive tic-tac-toe game made with ReactJS, based on the [introductory ReactJS tutorial](https://reactjs.org/tutorial/tutorial.html) on the official website.

**Added functionality:**

- The location on the grid of each move is displayed in the format `(col, row)` in the move history list;

- The currently selected item in the move list is bolded;

- The `Board` component has been rewritten to use loops to make the squares, instead of hardcoding them;

- Toggle button to sort the move history list in ascending or descending order;

- When a player wins, the three winning squares are highlighted;

- When no player wins, a message is displayed about the result being a draw.